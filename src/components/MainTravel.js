import React, {Component} from 'react';
import Sidebar from "./Sidebar/Sidebar";
import axios from "axios";
import {BASE_URL} from "./Constant/Constant";
import {Redirect} from 'react-router-dom';
import CardPlansTravel from "./Main/CardPlansTravel";

class MainTravel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            dataPlan: [],
            dataProvince: [],
            loading: true,
            err: false,
            urlGambar: '',
            id_hapus: '',
            index: 0,
        };
        document.body.style.backgroundImage = "none";
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        if (localStorage.getItem("profil") !== null) {
            axios({
                baseURL: BASE_URL,
                method: 'POST',
                url: '/user/check',
                data: {
                    token: JSON.parse(localStorage.getItem("profil")).token
                }
            }).then(resp => {
                console.log("asdsalkjasdl;lsdvsdo", resp);
                if (resp.data.login !== true) {
                    this.setState({
                        redirect: true
                    });
                    this.props.gantiNama('');
                }
            }).catch(err => {
                console.log(err)
            });
            axios({
                baseURL: BASE_URL,
                url: '/plan/get/travelid',
                method: 'POST'
            }).then(resp => {
                console.log("respontra", resp);
                this.setState({
                    dataPlan: this.state.dataPlan.concat(resp.data.data),
                    loading: false
                });
            }).catch(() => {
                this.setState({
                    err: true
                });
            });
        } else {
            this.setState({
                redirect: true
            });
        }
    }

    getDataAgain() {
        this.setState({
            err: false
        });
        axios({
            baseURL: BASE_URL,
            url: '/plan/get',
            method: 'POST',
            data: {
                id_user: JSON.parse(localStorage.getItem("profil")).data._id
            }
        }).then(resp => {
            this.setState({
                dataPlan: this.state.dataPlan.concat(resp.data),
                loading: false
            })
        }).catch(() => {
            this.setState({
                err: true
            });
        });
        axios({
            baseURL: BASE_URL,
            url: '/api/provinsi'
        }).then(resp => {
            this.setState({
                dataProvince: this.state.dataProvince.concat(resp.data)
            });
        }).catch(err => {
            console.log(err)
        })
    }

    forceRender(data) {
        this.setState({
            dataPlan: [data, ...this.state.dataPlan],
        });
        document.getElementById("buttonHideModal").click()
    }

    removeItem(index) {
        this.setState((prevState) => ({
            dataPlan: prevState.dataPlan.filter((_, i) => i !== index)
        }));
        if (document.getElementById("buttonModalDeleteClose") !== null) {
            document.getElementById("buttonModalDeleteClose").click();
        } else {
            const elements = document.getElementsByClassName("modal-open");
            while (elements.length > 0) {
                elements[0].classList.remove("modal-open");
            }
            const elements2 = document.getElementsByClassName("modal-backdrop");
            while (elements2.length > 0) {
                elements2[0].classList.remove("modal-backdrop");
            }
        }

    }

    stateDelete(id_hapus, index) {
        this.setState({
            id_hapus,
            index
        })
    }

    renderIsi() {
        if (this.state.loading && !this.state.err) {
            return (
                <div className="lds-ripple">
                    <div></div>
                    <div></div>
                </div>
            )
        } else if (this.state.loading && this.state.err) {
            return (
                <div className="errorPage" style={{height: window.innerHeight}}>
                    <h4>Error, Check Your Internet Connection</h4>
                    <button className="btn btn-danger" onClick={() => this.getDataAgain()}>Try Again</button>
                </div>
            )
        } else {
            return (
                <div className="row">
                    {this.state.dataPlan.map((a, i) => {
                        console.log("asdasdsa", a);
                        return (
                            <div className="col-md-4" key={i}>
                                <CardPlansTravel TanggalAwal={a.TanggalAwal} TanggalAkhir={a.TanggalAkhir}
                                                 stateDelete={(id_hapus, index) => this.stateDelete(id_hapus, index)}
                                                 bg={a.urlGambar} title={a.Destinasi} a={a} index={i}
                                                 idDestinasi={a._id}/>
                            </div>
                        )
                    })}
                </div>
            )
        }
    }

    render() {
        if (this.state.redirect) {
            this.props.gantiNama('');
            localStorage.removeItem("profil");
            return (
                <Redirect to={{pathname: '/login'}}/>
            )
        }
        return (
            <div style={{background: "#eee", height: "100vh"}}>
                <Sidebar gantiLagi={nama => this.props.gantiNama(nama)}/>
                <div className="containerMain">
                    <h2>Travel Dashboard</h2>
                    {this.renderIsi()}
                </div>
            </div>
        );
    }
}

export default MainTravel