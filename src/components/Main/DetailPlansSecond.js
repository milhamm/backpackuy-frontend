import React, {Component} from 'react';
import Sidebar from "../Sidebar/Sidebar";
import GridLayout from 'react-grid-layout';
import axios from "axios";
import {BASE_URL} from "../Constant/Constant";
import {Link, Redirect} from "react-router-dom";
import RightBar from "../Sidebar/RightBar";
import CurrencyFormat from "react-currency-format";
import ModalTicket from "./ModalTicket";
import ModalFindTravel from "./ModalFindTravel";
import moment from "moment";


class DetailPlansSecond extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idPlan: window.location.pathname.split("/")[2],
            redirect: false,
            loading: true,
            layout: [],
            layoutTemp: [],
            namaTravel: [],
            namaWisata: '',
            TanggalAkhir: null,
            index: 0,
            idAgenda: '',
            emptyRight: true,
            loadingRight: true,
            namaDestinasi: [],
            info: [],
            harga: [],
            days: [],
            day1: [],
            day2: [],
            day3: [],
            day4: [],
            qrCode: [],
            durasi: 6,
            draggable: true,
            travelId: "",
            width: window.innerWidth
        };
        this.layoutChange = this.layoutChange.bind(this);
        const anu = new Array(4).map((a, i) => {
            return i
        });
        console.log(anu)
    }

    updateDimensions() {
        this.setState({
            width: window.innerWidth
        });
    }

    componentWillMount() {
        this.updateDimensions.bind(this);
    }

    componentDidMount() {

        window.addEventListener("resize", this.updateDimensions.bind(this));
        if (localStorage.getItem("profil")) {
            axios({
                baseURL: BASE_URL,
                method: 'POST',
                url: '/user/check',
                data: {
                    token: JSON.parse(localStorage.getItem("profil")).token
                }
            }).then(resp => {
                if (!resp.data.login) {
                    this.setState({
                        redirect: true
                    });
                }
            }).catch(err => {
                console.log(err)
            });
            //Find Travel
            axios({
                baseURL: BASE_URL,
                url: 'user/find/travel',
                method: 'POST'
            }).then(resp => {
                console.log(resp.data.data);
                this.setState({
                    namaTravel: this.state.namaTravel.concat(resp.data.data)
                });
            });

            axios({
                baseURL: BASE_URL,
                url: 'plan/get/detail',
                method: 'POST',
                data: {
                    _id: this.state.idPlan
                }
            }).then(resp => {
                console.log("Plan get Derail", resp);
                this.setState({
                    namaWisata: resp.data.data.Destinasi,
                    durasi: resp.data.data.durasi,
                    travelId: resp.data.data.travelID,
                    TanggalAkhir: resp.data.data.TanggalAkhir
                });
            });
            axios({
                baseURL: BASE_URL,
                url: 'agenda/list',
                method: 'POST',
                data: {
                    id_detail: this.state.idPlan
                }
            }).then(resp => {

                this.setState({
                    layout: this.state.layout.concat(resp.data.data.position.map(a => {
                        return {
                            i: a.i, x: a.x, y: a.y, w: a.w, h: a.h, minW: 1, minH: 1, maxH: 2
                        }
                    })),
                    namaDestinasi: this.state.namaDestinasi.concat(resp.data.data.position.map(a => {
                        return a.Destinasi
                    })),
                    qrCode: this.state.qrCode.concat(resp.data.data.position.map(a => (
                        a.qr
                    ))),
                    harga: this.state.harga.concat(resp.data.data.position.map(a => {
                        return a.Harga
                    })),
                    idAgenda: resp.data.data._id,
                    loading: false
                });
                // const isafter = moment(moment()._d).isAfter(this.state.TanggalAkhir._d);
                console.log("Tanggal Akhir", this.state.TanggalAkhir);

                var isafter = moment("2014-03-23T01:14:00Z").isAfter('2014-03-24T01:14:00Z');
                console.log("isafter", isafter);
                if (isafter) {
                    this.setState({
                        draggable: false
                    })
                }
            }).catch(err => {
                console.log(err)
            })

        } else {
            this.setState({
                redirect: true
            });
        }
    }

    changeQR(qr) {

        axios({
            baseURL: BASE_URL,
            url: 'agenda/update',
            method: 'POST',
            data: {
                _id: this.state.idAgenda,
                position: this.state.layoutTemp.map((a, i) => {
                    return {
                        "Destinasi": this.state.namaDestinasi[i],
                        "Harga": this.state.harga[i],
                        "qr": qr,
                        "i": a.i,
                        "x": a.x,
                        "y": a.y,
                        "w": a.w,
                        "h": a.h
                    }
                })
            }
        });
        this.setState({
            qrCode: qr
        })
    }


    layoutChange(layout) {
        if (!this.state.loading) {
            this.setState({
                layoutTemp: layout
            });
            axios({
                baseURL: BASE_URL,
                url: 'agenda/update',
                method: 'POST',
                data: {
                    _id: this.state.idAgenda,
                    position: layout.map((a, i) => {
                        return {
                            "Destinasi": this.state.namaDestinasi[i],
                            "Harga": this.state.harga[i],
                            "qr": this.state.qrCode[i],
                            "i": a.i,
                            "x": a.x,
                            "y": a.y,
                            "w": a.w,
                            "h": a.h
                        }
                    })
                }
            }).then(async resp => {
                const position = resp.data.data.position;

                try {
                    let jumlah = Array.apply(null, Array(this.state.durasi)).map((a, i) => {
                        return position.filter((position) => {
                            return position.x === i;
                        }).map(a => {
                            return a.Harga
                        });
                    });
                    this.setState({
                        days: jumlah
                    });
                } catch (e) {
                    console.log(e)
                }
            })
        }

    }

    useTravel() {
        this.setState({
            travelId: "oke"
        })
    }

    detailClicked(destinasi, index) {
        this.setState({
            loadingRight: true,
        });
        axios({
            baseURL: BASE_URL,
            url: '/info/get',
            method: 'POST',
            data: {
                nama: destinasi
            }
        }).then(resp => {
            this.setState({
                info: resp.data.data,
                loadingRight: false,
                emptyRight: false,
                index
            })
        })
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {
        console.log(this.state.travelId);
        if (this.state.redirect) {
            return (
                <Redirect to={{pathname: '/login'}}/>
            )
        }
        if (this.state.loading) {
            return (
                <div className="lds-ripple">
                    <div></div>
                    <div></div>
                </div>
            )
        } else {
            document.body.style.backgroundColor = "#eee";
            console.log(this.state.travelId);
            return (
                <div style={{background: "#eee", minHeight: "100vh"}}>
                    <Sidebar gantiLagi={nama => this.props.gantiNama(nama)}/>
                    <RightBar index={this.state.index} qr={this.state.qrCode} empty={this.state.emptyRight}
                              loading={this.state.loadingRight} hideButton={this.state.draggable}
                              info={this.state.info}/>
                    <div className="containerMain">

                        <section className="headerDetail">
                            <Link to="/dashboard">
                                <img src="" alt=""/>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="#5e5e5e" width="30" height="30"
                                     viewBox="0 0 24 24">
                                    <path
                                        d="M2.117 12l7.527 6.235-.644.765-9-7.521 9-7.479.645.764-7.529 6.236h21.884v1h-21.883z"/>
                                </svg>
                            </Link>

                        </section>
                        <div style={this.state.width < 960 ? {} : {maxWidth: this.state.width - 520}}>
                            <div className="row">
                                <div className="col-12 dayTotal">
                                    <p>Total <CurrencyFormat value={this.state.harga.reduce((a, b) => a + b, 0)}
                                                             displayType={'text'} thousandSeparator={true}
                                                             prefix={'Rp'}/></p>
                                </div>
                            </div>
                        </div>
                        <div className="headerTitle"
                             style={this.state.width < 960 ? {} : {maxWidth: this.state.width - 520}}>
                            <h2>{this.state.namaWisata} Plan Trip {!this.state.draggable ?
                                <span class="badge badge-success">Completed Trip</span> : null}</h2>
                        </div>
                        <div style={this.state.width < 960 ? {} : {maxWidth: this.state.width - 520}}>
                            <div className="row">
                                <div className="col-12">
                                    {this.state.travelId !== "null" ?
                                        <button className="btn btn-success" data-toggle="modal"
                                                data-target="#modalFindTravel" disabled>Using Travel Agency</button> :
                                        <button className="btn btn-primary" data-toggle="modal"
                                                data-target="#modalFindTravel">Use Travel Agency</button>}
                                    <div style={{marginLeft: "16px"}} className="btn btn-outline-primary"
                                         data-toggle="modal" data-target="#modalAnu">Buy All Ticket
                                    </div>
                                </div>
                            </div>
                        </div>
                        <section className="mainDetail"
                                 style={this.state.width < 960 ? {} : {maxWidth: this.state.width - 520}}>
                            <div className="row" style={{minWidth: 1920}}>

                                {this.state.days.map((a, i) => {
                                    return (
                                        <div className="col" key={i}>
                                            <div className="dayPlanner">
                                                <h2>Day {i + 1}</h2>
                                                <p><CurrencyFormat value={this.state.days[i].reduce((a, b) => a + b, 0)}
                                                                   displayType={'text'} thousandSeparator={true}
                                                                   prefix={'Rp'}/>
                                                </p>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                            <GridLayout classname="layout container" layout={this.state.layout} cols={this.state.durasi}
                                        rowHeight={200}
                                        width={1920}
                                        isDraggable={this.state.draggable}
                                        margin={[16, 16]}
                                        isResizable={false}
                                        onLayoutChange={(layout) => this.layoutChange(layout)}>
                                {this.state.layout.map((a, i) => {
                                    return (
                                        <div key={a.i}>
                                            <div className="cardPlanTimeline">
                                                <div className="bgCardPlan">
                                                </div>
                                                <span>{this.state.namaDestinasi[i]}</span>
                                                <button className="btn btn-primary"
                                                        onClick={() => this.detailClicked(this.state.namaDestinasi[i], i)}>Detail
                                                </button>
                                            </div>
                                        </div>
                                    )
                                })}
                            </GridLayout>
                            <ModalTicket index={this.state.index} qr={this.state.qrCode}
                                         qrCode={(qr) => this.changeQR(qr)}
                                         total={this.state.harga.reduce((a, b) => a + b, 0)}/>
                            <ModalFindTravel idPlan={this.state.idPlan} travel={this.state.namaTravel}
                                             total={this.state.harga.reduce((a, b) => a + b, 0) + 290000}
                                             useTravel={() => this.useTravel()}/>
                        </section>

                    </div>

                </div>
            );
        }
    }
}

export default DetailPlansSecond

