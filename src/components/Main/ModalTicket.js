import React, {Component} from 'react';
import CurrencyFormat from "react-currency-format";

class ModalTicket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            method: ["Mandiri", "BNI", "BRI", "BTN", "CIMB Niaga"],
            toConfirm: false,
            jenisBayar: '',
        }
    }

    renderModal() {
        if (!this.state.toConfirm) {
            return (
                <div className="container containerProvinsi">
                    <div className="row">
                        {this.state.method.map((a, i) => {
                            return (
                                <div className="col-md-4 addPlanText" key={i} onClick={() => {
                                    this.setState({
                                        toConfirm: true,
                                        jenisBayar: a
                                    })
                                }}>
                                    <p>{a}</p>
                                </div>
                            );
                        })}
                    </div>
                </div>
            )
        } else {
            return (
                <div className="container containerProvinsi">
                    <div className="row">
                        <div className="col-6">
                            <div className="travelConfirm">
                                <h5>Name</h5>
                                <span>{JSON.parse(localStorage.getItem("profil")).data.fullName}</span>
                            </div>
                            <div className="travelConfirm">
                                <h5>Payment Method</h5>
                                <span>{this.state.jenisBayar}</span>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="travelConfirm">
                                <h5>No Rekening</h5>
                                <span>6680-1470-2334</span>
                            </div>
                            <div className="travelConfirm">
                                <h5>Total Payment</h5>
                                <span style={{fontWeight: 700}}>
                                    <CurrencyFormat value={this.props.total}
                                                    displayType={'text'} thousandSeparator={true}
                                                    prefix={'Rp'}/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <button className="btn btn-primary" style={{float: "right", width: "140px"}} onClick={() => {
                        this.props.qrCode("mantapgan");
                        document.getElementById("buttonHideModal").click()
                    }}>Confirm
                    </button>
                </div>
            )
        }

    }
    render() {
        return (
            <div className="modal fade" id="modalAnu" role="dialog" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content modalContainer">
                        <div className="bgAddPlan"
                             style={{background: `url("https://images.unsplash.com/photo-1497302347632-904729bc24aa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=144d48440ee329f1d475ac5db69d66fc&auto=format&fit=crop&w=1350&q=80")`}}>
                            <button type="button" id="buttonHideModal" className="close" data-dismiss="modal"
                                    aria-label="Close" onClick={() => {
                            }}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="contentAddPlan">
                            <h1>{"Payment Method"}</h1>
                        </div>
                        {this.renderModal()}
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalTicket