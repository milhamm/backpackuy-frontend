import React, {Component} from 'react';
import {Link} from "react-router-dom";
import moment from "moment";

class CardPlans extends Component {
    componentDidMount() {
        const isafter = moment(moment()).isAfter(this.props.TanggalAkhir);
        console.log(isafter)
    }

    renderBadge() {
        const isafter = moment(moment()).isAfter(this.props.TanggalAkhir);
        const isBefore = moment(moment()).isBefore(this.props.TanggalAkhir);
        if (isafter) {
            return (
                <h5><span className="badge badge-success" style={{position: "absolute"}}>Completed</span></h5>
            )
        } else if (isBefore) {
            return (
                <h5><span className="badge badge-secondary" style={{position: "absolute"}}>Upcoming</span></h5>
            )
        } else {
            return (
                <h5><span className="badge badge-primary" style={{position: "absolute"}}>On going</span></h5>
            )
        }
    }
    render() {
        const {idDestinasi, index, title, stateDelete} = this.props;
        return (
            <div className="cardPlans">
                <button type="button" className="close" style={{margin: "8px"}} data-toggle="modal"
                        data-target="#modalConfirmDelete" onClick={() => {
                    stateDelete(idDestinasi, index);
                }}>
                    <span aria-hidden="true">&times;</span>
                </button>
                {this.renderBadge()}
                <Link to={{pathname: `/dashboard/${idDestinasi}`}}>
                    <div className="plansBg" style={{background: `url("${this.props.bg}")`}}>

                    </div>
                    <div className="plansBg2">

                    </div>
                    <div className="textPlans">
                        <h4>{title}</h4>
                    </div>
                    <div className="textInfoPlans">
                        <div className="row no-gutters">
                            <div className="col-5">
                                From
                            </div>
                            <div className="col-2">

                            </div>
                            <div className="col-5">
                                To
                            </div>
                        </div>
                        <div className="row no-gutters">
                            <div className="col-5">
                                {moment(this.props.TanggalAwal).format("DD-MM-YYYY")}
                            </div>
                            <div className="col-2">
                                -
                            </div>
                            <div className="col-5">
                                {moment(this.props.TanggalAkhir).format("DD-MM-YYYY")}
                            </div>
                        </div>
                    </div>
                </Link>
            </div>
        );
    }
}
export default CardPlans