import React, {Component} from 'react';
import axios from 'axios';
import {BASE_URL} from "../Constant/Constant";
import CurrencyFormat from "react-currency-format";

class ModalFindTravel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataProps: {},
            namaTravel2: ["Lepas Suntuk"],
            namaTravel: [],
            pembayaran: ["Mandiri", "BCA", "BNI"],
            jenisBayar: '',
            toPembayaran: false,
            toConfirm: false
        }
    }

    componentDidMount() {
        //Find Travel
        axios({
            baseURL: BASE_URL,
            url: 'user/find/travel',
            method: 'POST'
        }).then(resp => {
            console.log(resp.data.data);
            this.setState({
                namaTravel: this.state.namaTravel.concat(resp.data.data)
            });

        });
    }

    confirmTravel() {

        axios({
            baseURL: BASE_URL,
            method: "POST",
            url: 'plan/update',
            data: {
                _id: this.props.idPlan,
                travelID: this.props.travel[0]._id
            }
        }).then(resp => {
            console.log(resp);
        }).catch(err => {
            console.error("Error Confirm Travel", err);
        })

    }

    renderIsiModal() {
        const {toPembayaran, toConfirm, pembayaran, namaTravel2} = this.state;
        if (!toPembayaran) {
            return (
                <div className="container containerProvinsi">
                    <div className="row">
                        {namaTravel2.map((a, i) => {
                            return (
                                <div className="col-md-4 travelPlan" key={i} onClick={() => {
                                    this.setState({
                                        namaTravel: a,
                                        toPembayaran: true,
                                    });
                                }}>
                                    <h6>{a}</h6>
                                    <p><CurrencyFormat value={this.props.total}
                                                       displayType={'text'} thousandSeparator={true}
                                                       prefix={'Rp'}/></p>
                                </div>
                            );
                        })}
                    </div>
                </div>
            )
        } else if (toPembayaran && !toConfirm) {
            return (
                <div className="container containerProvinsi">
                    <div className="row">
                        {pembayaran.map((a, i) => {
                            return (
                                <div className="col-md-4 addPlanText" key={i} onClick={() => {
                                    this.setState({
                                        jenisBayar: a,
                                        toConfirm: true,
                                    });
                                }}>
                                    <p>{a}</p>
                                </div>
                            )
                        })}
                    </div>
                </div>
            )
        } else {
            return (
                <div className="container containerProvinsi">
                    <div className="row">
                        <div className="col-6">
                            <div className="travelConfirm">
                                <h5>Name</h5>
                                <span>Muhammad Ilham Mubarak</span>
                            </div>
                            <div className="travelConfirm">
                                <h5>Payment Method</h5>
                                <span>{this.state.jenisBayar}</span>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="travelConfirm">
                                <h5>No Rekening</h5>
                                <span>6680-1470-2334</span>
                            </div>
                            <div className="travelConfirm">
                                <h5>Total Payment</h5>
                                <span style={{fontWeight: 700}}>
                                    <CurrencyFormat value={this.props.total}
                                                    displayType={'text'} thousandSeparator={true}
                                                    prefix={'Rp'}/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <button className="btn btn-primary" style={{float: "right", width: "140px"}} onClick={() => {
                        document.getElementById("btnHideModal").click();
                        this.confirmTravel();
                        this.props.useTravel();
                    }}>Confirm
                    </button>
                </div>
            )
        }
    }

    renderJudul() {
        if (!this.state.toPembayaran) {
            return "What Travel Agency?"
        } else if (this.state.toPembayaran && !this.state.toConfirm) {
            return "Payment Method"
        } else {
            return "Confirmation"
        }
    }

    render() {
        return (
            <div className="modal fade" id="modalFindTravel" tabIndex="-1" role="dialog" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content modalContainer">

                        <div className="bgAddPlan"
                             style={{background: `url("https://images.unsplash.com/photo-1470214203634-e436a8848e23?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c23e35c1a3badd6dd7de312b0894839d&auto=format&fit=crop&w=1491&q=80")`}}>
                            <button type="button" id="btnHideModal" className="close" data-dismiss="modal"
                                    aria-label="Close" onClick={() => {
                                this.setState({
                                    dataProps: {},
                                    namaTravel2: ["Lepas Suntuk"],
                                    namaTravel: [],
                                    pembayaran: ["Mandiri", "BCA", "BNI"],
                                    jenisBayar: '',
                                    toPembayaran: false,
                                    toConfirm: false
                                });
                            }}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="contentAddPlan">
                            <h1>{this.renderJudul()}</h1>
                        </div>
                        {this.renderIsiModal()}
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalFindTravel