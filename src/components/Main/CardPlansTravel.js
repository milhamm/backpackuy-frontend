import React, {Component} from 'react';
import {Link} from "react-router-dom";
import moment from "moment";

class CardPlansTravel extends Component {
    render() {
        const {idDestinasi, index, title, stateDelete} = this.props;
        return (
            <div className="cardPlans">
                <Link to={{pathname: `/travel/${idDestinasi}`}}>
                    <div className="plansBg" style={{background: `url("${this.props.bg}")`}}>

                    </div>
                    <div className="plansBg2">

                    </div>
                    <div className="textPlans">
                        <h4>{title}</h4>
                    </div>
                    <div className="textInfoPlans">
                        <div className="row">
                            <div className="col-6">
                                <div className="row no-gutters">
                                    <div className="col-5">
                                        From
                                    </div>
                                    <div className="col-2">

                                    </div>
                                    <div className="col-5">
                                        To
                                    </div>
                                </div>
                                <div className="row no-gutters">
                                    <div className="col-5">
                                        {moment(this.props.TanggalAwal).format("DD-MM-YYYY")}
                                    </div>
                                    <div className="col-2">
                                        -
                                    </div>
                                    <div className="col-5">
                                        {moment(this.props.TanggalAkhir).format("DD-MM-YYYY")}
                                    </div>
                                </div>
                            </div>
                            <div className="col-6 textPlanTravel">
                                <span>Ordered By</span>
                                <p>Muhammad Ilham Mubarak</p>
                            </div>
                        </div>

                    </div>
                </Link>
            </div>
        );
    }
}

export default CardPlansTravel