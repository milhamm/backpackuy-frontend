import React, {Component} from 'react';
import axios from 'axios';
import {BASE_URL} from "../Constant/Constant";
import {SingleDatePicker} from 'react-dates';
import moment from 'moment';

class ModalAddPlans extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataProps: {},
            provinsi: '',
            kota: '',
            tanggalAwal: '',
            tanggalAkhir: '',
            createdOn: '',
            id_detail: '',
            toKota: false,
            toPilihDurasi: false,
            toPilihDate: false,
            urlGambar: '',
            durasi: 0,
            date: null,
            dataTemp: null,
            focused: false,
        }
    }

    renderIsiModal() {
        const hari = [1, 2, 3, 4, 5, 6];
        const {provinsi, kota, tanggalAwal, toKota, dataProps, toPilihDurasi, toPilihDate} = this.state;
        if (provinsi === '' && kota === '' && tanggalAwal === '') {
            return (
                <div className="container containerProvinsi">
                    <div className="row">
                        {this.props.dataProvince.map((a, i) => {
                            return (
                                <div className="col-md-4 addPlanText" key={i} onClick={() => {
                                    this.setState({
                                        provinsi: a.Provinsi,
                                        toKota: true,
                                        dataProps: {...dataProps[i], a}
                                    });
                                }}>
                                    <p>{a.Provinsi}</p>
                                </div>
                            );
                        })}
                    </div>
                </div>
            )
        } else if (toKota && !toPilihDate) {
            return (
                <div className="container containerProvinsi">
                    <div className="row">
                        {this.state.dataProps.a.Kota.map((a, i) => {
                            return (
                                <div className="col-md-4 addPlanText" key={i} onClick={() => {
                                    this.setState({
                                        kota: a.Nama,
                                        toPilihDate: true
                                    });
                                }}>
                                    <p>{a.Nama}</p>
                                </div>
                            )
                        })}
                    </div>
                </div>
            )
        } else if (toPilihDate && !toPilihDurasi) {
            return (
                <div className="container containerProvinsi">
                    <SingleDatePicker
                        block={true}
                        // isOutsideRange={day => (moment().diff(day) < 0)}
                        date={this.state.date} // momentPropTypes.momentObj or null
                        onDateChange={date => this.setState({date, toPilihDurasi: true})} // PropTypes.func.isRequired
                        focused={this.state.focused} // PropTypes.bool
                        onFocusChange={({focused}) => this.setState({focused})} // PropTypes.func.isRequired
                        id="dataPicker" // PropTypes.string.isRequired,
                    />
                </div>
            )
        } else if (toPilihDurasi) {
            return (
                <div className="container containerProvinsi">
                    <div className="row">
                        {hari.map((a, i) => {
                            return (
                                <div className="col-md-4 addPlanText" key={i} onClick={() => {
                                    this.setState({
                                        durasi: i
                                    });
                                    if (localStorage.getItem("profil")) {
                                        axios({
                                            baseURL: BASE_URL,
                                            url: '/plan/add',
                                            method: 'POST',
                                            data: {
                                                id_user: JSON.parse(localStorage.getItem("profil")).data._id,
                                                Destinasi: this.state.kota,
                                                durasi: i + 1,
                                                urlGambar: this.props.urlGambar.toString(),
                                                TanggalAwal: this.state.date._d,
                                                TanggalAkhir: moment(this.state.date._d).add(i + 1, 'days')._d,
                                            }
                                        }).then(resp => {
                                            console.log("add plans", resp.data);
                                            this.setState({
                                                id_detail: resp.data._id
                                            });
                                            axios({
                                                baseURL: BASE_URL,
                                                method: 'POST',
                                                url: '/info/get',
                                                data: {
                                                    kota: this.state.kota
                                                }
                                            }).then(resp => {
                                                axios({
                                                    baseURL: BASE_URL,
                                                    method: 'POST',
                                                    url: '/agenda/insert',
                                                    data: {
                                                        id_detail: this.state.id_detail,
                                                        position: resp.data.data.map((a, i) => {
                                                            return {
                                                                Destinasi: a.nama,
                                                                Harga: a.tiket,
                                                                qr: '',
                                                                i: a._id,
                                                                x: i < this.state.durasi + 1 ? i : i,
                                                                y: i < this.state.durasi + 1 ? 0 : 1,
                                                                w: 1,
                                                                h: 1
                                                            }
                                                        })
                                                    }
                                                }).then(resp => {
                                                    this.props.forceUpdate(this.state.dataTemp);
                                                })
                                            });
                                            this.setState({
                                                dataTemp: resp.data
                                            })

                                        }).catch(err => {
                                            console.log(err)
                                        });
                                    }
                                }}>
                                    <p>{i + 1 + " Day Trip"}</p>
                                </div>
                            )
                        })}
                    </div>
                </div>
            )
        }
    }

    renderJudul() {
        if (this.state.toPilihDate && !this.state.toPilihDurasi) {
            return "From When?"
        } else if (this.state.toPilihDate) {
            return "How Long?"
        } else {
            return "Where Would You Like To Go?"
        }
    }

    render() {
        const {urlGambar} = this.props;
        return (
            <div className="modal fade" id="modalAddPlans" tabIndex="-1" role="dialog" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content modalContainer">

                        <div className="bgAddPlan" style={{background: `url("${urlGambar}")`}}>
                            <button type="button" id="buttonHideModal" className="close" data-dismiss="modal"
                                    aria-label="Close" onClick={() => {
                                this.setState({
                                    dataProps: {},
                                    provinsi: '',
                                    kota: '',
                                    tanggalAwal: '',
                                    tanggalAkhir: '',
                                    createdOn: '',
                                    id_detail: '',
                                    toKota: false,
                                    toPilihDurasi: false,
                                    toPilihDate: false,
                                    urlGambar: '',
                                    durasi: 0,
                                    date: null,
                                    dataTemp: null,
                                    focused: false,
                                });
                            }}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="contentAddPlan">
                            <h1>{this.renderJudul()}</h1>
                        </div>
                        {this.renderIsiModal()}
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalAddPlans