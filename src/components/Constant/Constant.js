export const colors = {
    primaryColor: "#fbc531",
    secondaryColor: ""
};

export const BASE_URL = 'https://api.backpackuy.com';
export const UNSPLASH_BASE_URL = 'https://api.unsplash.com/';


export const landingFeatures = [
    {
        title: "Recommended Places",
        desc: "Wisata yang sedang populer, itulah yang direkomendasikan, tetapi juga memperhatikan beberapa faktor.",
        img: "map.png"
    },
    {
        title: "Scheduler",
        desc: "Membuat agenda perjalanan yang fleksibel terhadap minat pengguna. Memberikan agenda pada setiap hari yang sudah ditentukan.",
        img: "calender.png"
    },
    {
        title: "Budget your Trip",
        desc: "Mengestimasi biaya yang diperlukan selama berwisata. Kemudahan dalam mengestimasi biaya yang harus dikeluarkan tanpa harus takut kekurangan uang.",
        img: "wallet.png"
    }
];
export const profile = [
    {
        nama: "Adhimas Waskita Ramadhana",
        desc: "adhimaswaskita7a@gmail.com",
        pic: "rama.jpg"
    },
    {
        nama: "Muhammad Ilham Mubarak",
        desc: "m.ilham.mubarak@gmail.com",
        pic: "aang.jpg"
    },
    {
        nama: "Riyo Sadewa",
        desc: "riyodewo05@gmail.com",
        pic: "riyo.jpg"
    }
];
export const landingCarousel = [
    {
        title: "Gunung Bromo",
        kota: "Kota Malang",
    },
    {
        title: "Jawa Timur Park 1",
        kota: "Kota Batu",
    },
    {
        title: "Paralayang",
        kota: "Kota Batu",
    },
    {
        title: "Taman Wisata Baluran",
        kota: "Kota Banyuwangi",
    },
    {
        title: "Candi Borobudur",
        kota: "Daerah Istimewa Yogyakarta",
    }
];